extends Node

signal checkpoint_changed (checkpoint)
signal win ()

var current_checkpoint: Node2D = null setget set_current_checkpoint

func set_current_checkpoint(_current_checkpoint: Node2D) -> void:
	if current_checkpoint != _current_checkpoint:
		current_checkpoint = _current_checkpoint
		emit_signal("checkpoint_changed", current_checkpoint)

func win() -> void:
	emit_signal("win")
