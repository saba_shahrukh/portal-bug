extends "res://addons/snopek_state_machine/State.gd"

onready var host = $"../.."

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Idle" if host.vector.x == 0 else "Run")

func _get_player_input_vector() -> Vector2:
	return Vector2(Input.get_action_strength("player_right") - Input.get_action_strength("player_left"), 0)

func _state_physics_process(delta: float) -> void:
	var input_vector = _get_player_input_vector()
	
	if Input.is_action_just_pressed("player_jump"):
		if host.is_on_floor():
			get_parent().change_state("PreJump", {
				"input_vector": input_vector,
			})
			return
		else:
			get_parent().change_state("Fly", {
				"input_vector": input_vector,
			})
			return
	elif input_vector != Vector2.ZERO:
		get_parent().change_state("Move", {
			"input_vector": input_vector,
		})
		return
	
	# Decelerate to 0.
	if host.vector.x < 0:
		host.vector.x = min(0.0, host.vector.x + (host.friction * delta))
	elif host.vector.x > 0:
		host.vector.x = max(0.0, host.vector.x - (host.friction * delta))
	
	# If we just decelerated to 0, then switch to the idle animation.
	if host.animation_player.current_animation != "Idle" and host.vector.x == 0:
		host.animation_player.play("Idle")
