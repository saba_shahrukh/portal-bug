extends "res://actors/player-states/Idle.gd"

var walk_sound

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Run")
	if info.has('input_vector'):
		do_move(info['input_vector'])

func _state_exit() -> void:
	if walk_sound:
		walk_sound.stop()

func _state_physics_process(delta: float) -> void:
	var input_vector = _get_player_input_vector()
	if Input.is_action_just_pressed("player_jump"):
		if host.is_on_floor():
			get_parent().change_state("PreJump", {
				"input_vector": input_vector,
			})
			return
		else:
			get_parent().change_state("Fly", {
				"input_vector": input_vector,
			})
			return
	elif input_vector == Vector2.ZERO:
		get_parent().change_state("Idle")
		return
	
	do_move(input_vector)
	
	if host.is_on_floor():
		if not walk_sound or not walk_sound.playing:
			walk_sound = host.sounds.play("Walk")
	else:
		if walk_sound and walk_sound.playing:
			walk_sound.stop()

func do_flip_sprite(input_vector: Vector2) -> void:
	if input_vector.x < 0:
		host.sprite.flip_h = true
	elif input_vector.x > 0:
		host.sprite.flip_h = false

func do_move(input_vector: Vector2) -> void:
	do_flip_sprite(input_vector)
	
	# Accelerate to top speed.
	var delta = get_physics_process_delta_time()
	if input_vector.x > 0:
		host.vector.x = min(input_vector.x * host.speed, host.vector.x + (host.acceleration * delta))
	elif input_vector.x < 0:
		host.vector.x = max(input_vector.x * host.speed, host.vector.x - (host.acceleration * delta))

