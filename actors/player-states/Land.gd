extends "res://actors/player-states/Move.gd"

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Land")

func _state_exit() -> void:
	host.animation_player.stop()

func _state_physics_process(delta: float) -> void:
	var input_vector = _get_player_input_vector()
	
	if Input.is_action_just_pressed("player_jump"):
		get_parent().change_state("PreJump", {
			"input_vector": input_vector,
		})
		return
	
	do_move(input_vector)

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == 'Land':
		get_parent().change_state('Idle')
