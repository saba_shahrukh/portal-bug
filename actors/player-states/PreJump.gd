extends "res://actors/player-states/Move.gd"

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("PreJump")
	if info.has('input_vector'):
		do_move(info['input_vector'])

func _state_exit() -> void:
	host.animation_player.stop()

func _state_physics_process(delta: float) -> void:
	#var input_vector = _get_player_input_vector()
	#do_move(input_vector)
	if Input.is_action_just_released("player_jump"):
		get_parent().change_state('Idle')
		return

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == 'PreJump':
		get_parent().change_state('Jump')
