extends "res://actors/player-states/Move.gd"

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Fall")
	#host.modulate = Color(1.0, 0.0, 0.0, 1.0)

func _state_exit() -> void:
	#host.modulate = Color(1.0, 1.0, 1.0, 1.0)
	pass

func _state_physics_process(delta: float) -> void:
	if host.is_on_floor():
		get_parent().change_state("Land")
		return
	
	if Input.is_action_just_released("player_down"):
		get_parent().change_state("Fall")
		return
	
	var input_vector = _get_player_input_vector()
	do_flip_sprite(input_vector)
	
	# Point the input vector down.
	input_vector.y = 1.0
	# Don't normalize this! It feels betten when the diagonal moves a little
	# bit further.
	#input_vector = input_vector.normalized()
	
	# Accelerate towards top speed.
	host.vector = host.vector.move_toward(input_vector * host.dive_speed, host.dive_acceleration * delta)
