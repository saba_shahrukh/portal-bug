extends "res://addons/snopek_state_machine/State.gd"

onready var host = $"../.."

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Hurt")
	host.sounds.play("Hurt")
	host.vector = host.vector.normalized() * -1 * host.push_back_speed

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "Hurt" and host.state_machine.current_state == self:
		host.state_machine.change_state("Idle")
