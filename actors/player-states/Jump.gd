extends "res://actors/player-states/Move.gd"

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Jump")
	host.sounds.play("Jump")
	host.vector.y = -host.jump_speed
	
	if info.has('input_vector'):
		do_move(info['input_vector'])

func _state_physics_process(delta: float) -> void:
	if host.is_on_floor():
		get_parent().change_state("Land")
		return
	
	var input_vector = _get_player_input_vector()
	do_move(input_vector)
	
	# If the player releases the jump key, then interrupt the jump.
	if host.vector.y < 0.0 and Input.is_action_just_released("player_jump"):
		host.vector.y = 0.0

	# If the player presses down, then we dive.
	if Input.is_action_just_pressed("player_down"):
		get_parent().change_state("Dive")
		return
	
	# If the player presses jump again, then we switch to the 'Fly' state.
	if Input.is_action_just_pressed("player_jump"):
		get_parent().change_state("Fly")
		return
	
	# If the character is descending by certain amount, then switch to the
	# 'Fall' state.
	#if host.vector.y >= 50.0:
	#	get_parent().change_state("Fall")
	#	return
	
	if host.vector.y >= 50.0:
		if Input.is_action_pressed("player_jump"):
			host.vector.y = 10.0
			if host.animation_player.current_animation != "Glide":
				host.animation_player.play("Glide")
		elif host.animation_player.current_animation != "Fall":
			host.animation_player.play("Fall")
		


