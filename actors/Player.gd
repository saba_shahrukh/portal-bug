extends KinematicBody2D
class_name Player

export (float) var speed := 400.0
export (float) var acceleration := 2000.0
export (float) var friction := 4000.0
export (float) var gravity := 800.0
export (float) var jump_speed := 400.0
export (float) var fly_speed := 700.0
export (float) var dive_speed := 800.0
export (float) var dive_acceleration := 40000.0
export (float) var terminal_velocity := 1600.0
export (float) var push_back_speed := 400.0

signal player_dead ()

onready var sprite := $Sprite
onready var animation_player := $AnimationPlayer
onready var state_machine := $StateMachine
onready var sounds := $Sounds

var vector := Vector2.ZERO

func reset_state() -> void:
	var current_state_name = state_machine.current_state.name if state_machine.current_state != null else "None"
	if current_state_name != "Idle" and current_state_name != "Teleport":
		state_machine.change_state("Idle")
	sprite.flip_h = false

func _ready() -> void:
	reset_state()

func _physics_process(delta: float) -> void:
	vector.y += (gravity * delta)
	if vector.y > terminal_velocity:
		vector.y = terminal_velocity
	vector = move_and_slide(vector, Vector2.UP)
	
	if position.y > 96:
		state_machine.change_state("Dead")
	
