extends StaticBody2D

func _on_Hurtbox_body_entered(body: Node) -> void:
	body.state_machine.change_state("Hurt")
